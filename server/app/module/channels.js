var _ = require('lodash'),
	config = require('./channels-config');

module.exports = function initChannelsApi(connection){	
	var router = require('./../back-router')(connection),
		redisWriteClient = require('./../redis')(),	
		redisSubscribeClient = require('./../redis')();
	
	redisSubscribeClient.on('message', function (channel, message) {
		var connectionIdIndex = message.indexOf(':');
		if(connectionIdIndex === -1){
			return;
		}

		// redis pubsub only for cross script interaction
		var connectionId = message.slice(0, connectionIdIndex);
		if(connection.id === connectionId) {
			return;
		}

		message = message.slice(connectionIdIndex + 1);
		channelsApi.addMessage([message], true);
	});

	var channelsApi = {
		get: function(channelRequests){
			channelRequests.forEach(function(channelRequestData){
				if(!channelRequestData.channel) {
					return;
				}

				redisWriteClient.lrange(channelRequestData.channel, -10, -1, function(error, messages){
					if(messages && messages.length) {
						router('messages', messages.reverse());
					}
				});				

				redisSubscribeClient.unsubscribe();
				redisSubscribeClient.subscribe(channelRequestData.channel + ':subscribtion');
			});
		},

		addMessage: function(messages, silent){
			_.each(_.groupBy(messages, 'channel'), function(messages, channel){				
				if(!silent) {
					var multi = redisWriteClient.multi();

					multi.expire(channel, config.EXPIRES);
					messages.forEach(function(message){
						message.timestamp = +new Date();
						var messageStr = JSON.stringify(message);
						multi.lpush(channel, messageStr);
						multi.publish(channel + ':subscribtion', connection.id + ':' + messageStr);
					});

					// there must be one element at all in messages here, by requirements
					multi.ltrim(channel, 0, config.MAX_LENGTH - 1);
					multi.exec(function(errors, results) {
						//router('messages', messages);
					});
				} else {
					router('messages', messages);	
				}
			});
		}
	};

	return channelsApi;
}