var socketServer = require('r2d2/server/vws.socket.js').server,
    routerInit = require('./router');

var ports = process.argv.slice(2);
ports.forEach(function(val, index, array) {
    socketServer('server-on-port:' + val, startSocketServer).config({
        port: +val
    });
});

function startSocketServer(connection, server) {
  var router = routerInit(connection);

  connection.on('open', function ( id ) {
      console.log('[open]');
  });

  connection.on('message', function ( msg ) {
      console.log('[message]', msg);  
      router(msg);    
  });

  connection.on('error', function ( err ) {
      console.log(err);
  });

  connection.on('close', function(){
      console.log('[close]');
  });
}