module.exports = function(connection){
	return function(command, data){
		connection.send(JSON.stringify({
			command: command,
			data: data
		}));
	}
}