var redis = require("redis")

module.exports = function(){
	var client = redis.createClient();

	client.on("error", function (err) {
	    console.log("Redis client error " + err);
	    client.quit();
	});

	return client;
};