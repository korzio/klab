var channelInit = require('./module/channels');

module.exports =  function(connection){
	var channels = channelInit(connection);

	return function(msg){
		try {
	      var messageData = JSON.parse(msg.utf8Data).action;
	    } catch(e){
	    	throw new Error('Error message data parse');
	    }

		if(!messageData) {
			throw new Error('Empty message data');
		}

		switch(messageData.command) {
			case 'join': 
				channels.get(messageData.data);				
				break;
			case 'msg':
				channels.addMessage(messageData.data);
				break;
		}
	};
};