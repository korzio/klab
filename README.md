# Backend and client for k-lab description

## First step - creating draft version 

I started from reading a book about Redis "The Little Redis Book" [1] by Karl Seguin.

Then i started to work on building chat - first, I downloaded resources that you have recommended. Then, I created a folder "app", that is general for the node application. I added two services - router and back-router, that are responsible for a cross client message passing. The router gets messages from a browser and then, if a callback is registered - sends it to an appropriate module. Back-router on the otherside sends messages to a client by a given command from a module. Also, I created the channels module, that keeps business logics for channels and messages - like creating list, adding messages to a list. It is responsible for a saving chats data in redis store.

It took me about 3 hours to finish the first step.

## Second step - cross channel

The next step was to create cross channel chat ability. I decided to split logic into 2 redis services. Both services are implemented in `module/channels` script.

The main part - is to add messages into collections. For these aim I use redis client called redisWriteClient. When a user connects to the server, it creates an instance of redisWriteClient, and remembers the pair - connection and client in a closure. When the message comes from this client, it goes through a simple router to a method `addMessage` in the `channelsApi`. But then it must notify other clients, that something happened. 

So, this is a second part - to notify other clients. Other redis service is used for that goal - `redisSubscribeClient`. It is a internal messages listener - only for server side. After adding new messages to database, `redisWriteClient` publishes a message to the channel with the specified id. And, when the connection is created and users connects to the specified channel, the `redisSubscribeClient` will subscribe to this channel. So, it will be notified, when the message has come. To identify this message from the same service instance, I decided to add a prefix with the connection id to the notifier message. It is not the best solution, but it could be the simpliest.

It took me about 3.5 hours to finish the second step.

## Technologies

1. Redis - PubSub pattern, List to keep channels data
2. Lodash - as utility for keeping clear logics.

# Starting the application

* Install dependencies & vendor dependencies
	~ cd klab/server
	~ npm install
	
* Run redis
	~ redis-server

* Start the appplication
	~ cd klab/server
	~ npm start

Default configuration for running server - is start two nodes on ports 2020 and 2021. You can run them manually in a way like this:

	~ node app/app 2020
	~ node app/app 2021
	~ ...

* Open chat page in a browser 
	~ file:///.../klab/client/client.html


[1] [The Little Redis Book](http://openmymind.net/redis.pdf) 